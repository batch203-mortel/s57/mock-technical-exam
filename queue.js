let collection = [];

// Write the queue functions below.
function print(){
       return collection;
}

function enqueue(name){
    collection[collection.length] = name;
     return collection;  
}

function dequeue(){
    let newCollection =[];
    
    for(let i=1; i < collection.length ; i++){
        
        newCollection[newCollection.length] = collection[i]

    }
    collection = newCollection;
    return collection;
}

function front(){
    return collection[0];
}

function size(){
    return collection.length;
}

function isEmpty(){
    if(collection.length === 0 ){
        return true;
    }
    else{
        return false;
    }
}






// Export create queue functions below.

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};